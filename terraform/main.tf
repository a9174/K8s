variable "vultr_token" {}

terraform {
  backend "http" {
  }
}

provider "vultr" {
  api_key = var.vultr_token
  rate_limit = 700
  retry_limit = 3
}

resource "vultr_ssh_key" "herbalway" {
  name       = "herbalway"
  ssh_key = "ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAQEAlMi7H+Q8EAEMg1OSXcGaE49Sf6yN1dIeE1Kg+x8Z9kQSb70do2nD4BrTaeTuWf2yWX2Z4Ns5RzXvQJyDQ1kmmvtI4cCWQCIanF+p/ZjbNUemsedqLDns+T1Nlxa5ic7f8lMoCVVowYscH2D6QnluLrCIgLfHk92Bz4sYp5jiF8ymDryxhJMipToW/ia2yPqV+GgyRXFgKO+qRnGgcYuC9vEkYnuqn33qmc+/Mar0a21p1NbVE5okueQqqrKN7rvPDLxa9HfPqI71sY4lswzq4Y1tThPi7uB4gIsKVs0k9mTOv5lNxNwyjj+EhKRKu7up4aSqACwWi3p9PgE3rQCOw=="
}

resource "vultr_instance" "server" {
    plan = "vc2-2c-4gb"
    region = "ewr"
    os_id = "517"
    label = "server"
    ssh_key_ids = [vultr_ssh_key.herbalway.id]
    private_network_ids = [vultr_private_network.k3s.id]
}

resource "vultr_instance" "agent-1" {
    plan = "vc2-2c-4gb"
    region = "ewr"
    os_id = "517"
    label = "agent-1"
    ssh_key_ids = [vultr_ssh_key.herbalway.id]
    private_network_ids = [vultr_private_network.k3s.id]
}

resource "vultr_instance" "agent-2" {
    plan = "vc2-2c-4gb"
    region = "ewr"
    os_id = "517"
    label = "agent-2"
    ssh_key_ids = [vultr_ssh_key.herbalway.id]
}

resource "vultr_private_network" "k3s" {
    description = "k3s private network"
    region = "ewr"
    v4_subnet  = "10.29.0.0"
    v4_subnet_mask = 24
}